/*

	Tile Keyboard CV

		thx2112

		ToDo:

		(All done.)

*/

#include <TinyWireM.h>
#include <TinyCAP1188.h>
#include <TinyMCP4726.h>
#include <EEPROM.h>

#define CAP1188_RESET  4

TinyCAP1188 tcap = TinyCAP1188(CAP1188_RESET);
TinyCAP1188 bcap = TinyCAP1188(CAP1188_RESET);

#define gate 3

TinyMCP4726 dac;

uint16_t level = 0;
byte temp;
long octave = 2;
long oldOctave = 2;
int octLED = 4;
int oldOctLED = 4;

uint8_t btouched;
uint8_t ttouched;

boolean keyDown = false;
boolean keyUp = false;
boolean keyLatch = false;
boolean latched = false;
boolean keyC = false;
boolean keyCs = false;
boolean keyD = false;
boolean keyDs = false;
boolean keyE = false;
boolean keyF = false;
boolean keyFs = false;
boolean keyG = false;
boolean keyGs = false;
boolean keyA = false;
boolean keyAs = false;
boolean keyB = false;
boolean newPress = false;

//
//	Calculations are done in microvolts for speed/accuracy. 1V = 1000000uV.
//

long scale = 0;
long shift = 0;			//	Volts to shift notes.
int note;
long vout;
int now;
int holdStart;

long over;

boolean testing = false;

int tunedNote[60];

long setPointTuned[6] = { 0,1000000,2000000,3000000,4000000,5000000 };
long setPointUntuned[6] = { 0,1000000,2000000,3000000,4000000,5000000 };
long adjust[6] = { 0,0,0,0,0,0 };

int eeAddress = 0;

void setup() {
	TinyWireM.begin();

	dac.begin(TinyMCP4726_DEFAULT_ADDR); //should be 96 or 0x60 or higher
	dac.setGain(TinyMCP4726_GAIN_1X);
	dac.setVReference(TinyMCP4726_VREF_VREFPIN);

	TinyWireM.beginTransmission(TinyMCP4726_DEFAULT_ADDR);
	TinyWireM.send(0x90);	//	Config mode 100, bits 100000
	TinyWireM.endTransmission();

	pinMode(gate, OUTPUT);

	bcap.begin(0x2b);
	tcap.begin(0x28);

	bcap.writeRegister(0x2A, 0x41);  // 0x2A default 0x80 use 0x41  � Set multiple touches back to off
	bcap.writeRegister(0x72, 0x00);  // 0x72 default 0x00  � Sets LED links back to off (default)
	bcap.writeRegister(0x28, 0x00);  // 0x28 default 0xFF use 0x00  � Turn off interrupt repeat on button hold

	tcap.writeRegister(0x2A, 0x41);  // 0x2A default 0x80 use 0x41  � Set multiple touches back to off
	tcap.writeRegister(0x72, 0x00);  // 0x72 default 0x00  � Sets LED links back to off (default)
	tcap.writeRegister(0x28, 0x00);  // 0x28 default 0xFF use 0x00  � Turn off interrupt repeat on button hold

	  //	Flash leds

	tcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
	bcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
	delay(100);
	tcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
	bcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);

	digitalWrite(gate, LOW);

	//	Latch LED

	delay(100);
	temp = bcap.readRegister(116);
	bitSet(temp, 7);
	bcap.writeRegister(116, int(temp));
	delay(100);
	temp = bcap.readRegister(116);
	bitClear(temp, 7);
	bcap.writeRegister(116, int(temp));


	//	Set up octave indicator
	bitSet(temp, int(octave + 2));
	bcap.writeRegister(116, int(temp));

	//	Load tuning
	eeAddress = 0;
	for (int i = 0; i < 60; i++) {
		EEPROM.get(eeAddress, tunedNote[i]);
		eeAddress += sizeof(int);
	}

	if (tunedNote[0] == -1) {	//	Preload defaults if first runthrough.
		testMode();
	}



}

void loop() {
	if (testing) { testMode(); }

	btouched = bcap.touched();
	ttouched = tcap.touched();

	//
	//	Simultaneously touch LATCH, UP, and DOWN to enter tuning mode.
	//

	if (bitRead(btouched, 5) && bitRead(btouched, 6) && bitRead(btouched, 7))
	{
		tcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
		bcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
		delay(100);

		testing = true;
		testMode();

		tcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
		bcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
	}

	//
	//	Octave Up/Down and lights.
	//
	if (bitRead(btouched, 6) && (!keyDown)) {				//	CS7
		keyDown = true;
		octave--;
		if (octave <= 0) { octave = 0; }
	}
	if (!bitRead(btouched, 6) && (keyDown)) {
		keyDown = false;
	}

	if (bitRead(btouched, 7) && (!keyUp)) {					//	CS8
		keyUp = true;
		octave++;
		if (octave >= 4) { octave = 4; }
	}
	if (!bitRead(btouched, 7) && (keyUp)) {
		keyUp = false;
	}

	//
	//	Octave LED indicators. Leftmost has highest port#.
	//

	if (oldOctave != octave) {
		octLED = (6 - octave);	//	Invert and shift by two to match port with physical LED location.
		temp = bcap.readRegister(116);
		bitClear(temp, int(oldOctLED));
		bitSet(temp, int(octLED));
		bcap.writeRegister(116, int(temp));
		oldOctave = octave;
		oldOctLED = octLED;
	}

	//
	//	Latch
	//
	if (bitRead(btouched, 5) && (!keyLatch))
	{
		keyLatch = true;
		if (holdStart == 0) {
			holdStart = millis();
		}
		now = millis() - holdStart;
		if (now >= 5000) {
			testing = true;
			testMode();
		}

		if (latched) { latched = false; }
		else if (!latched) {
			latched = true;
		}
		temp = bcap.readRegister(116);
		if (latched) {
			bitSet(temp, 7);
		}
		if (!latched) {
			bitClear(temp, 7);
		}
		bcap.writeRegister(116, int(temp));
	}

	if (!bitRead(btouched, 5) && keyLatch) {
		keyLatch = false;
		holdStart = 0;
	}

	//
	//	Notes
	//

	if (bitRead(ttouched, 0) && (!keyC)) {
		keyC = true;
		note = 0;
		newPress = true;
	}
	if (!bitRead(ttouched, 0) && (keyC)) {
		keyC = false;
		newPress = false;
	}

	if (bitRead(ttouched, 1) && (!keyCs)) {
		keyCs = true;
		note = 1;
		newPress = true;
	}
	if (!bitRead(ttouched, 1) && (keyCs)) {
		keyCs = false;
		newPress = false;
	}

	if (bitRead(ttouched, 3) && (!keyD)) {
		keyD = true;
		note = 2;
		newPress = true;
	}
	if (!bitRead(ttouched, 3) && (keyD)) {
		keyD = false;
		newPress = false;
	}

	if (bitRead(ttouched, 2) && (!keyDs)) {
		keyDs = true;
		note = 3;
		newPress = true;
	}
	if (!bitRead(ttouched, 2) && (keyDs)) {
		keyDs = false;
		newPress = false;
	}

	if (bitRead(ttouched, 4) && (!keyE)) {
		keyE = true;
		note = 4;
		newPress = true;
	}
	if (!bitRead(ttouched, 4) && (keyE)) {
		keyE = false;
		newPress = false;
	}

	if (bitRead(btouched, 4) && (!keyF)) {
		keyF = true;
		note = 5;
		newPress = true;
	}
	if (!bitRead(btouched, 4) && (keyF)) {
		keyF = false;
		newPress = false;
	}

	if (bitRead(ttouched, 5) && (!keyFs)) {
		keyFs = true;
		note = 6;
		newPress = true;
	}
	if (!bitRead(ttouched, 5) && (keyFs)) {
		keyFs = false;
		newPress = false;
	}

	if (bitRead(btouched, 3) && (!keyG)) {
		keyG = true;
		note = 7;
		newPress = true;
	}
	if (!bitRead(btouched, 3) && (keyG)) {
		keyG = false;
		newPress = false;
	}

	if (bitRead(ttouched, 6) && (!keyGs)) {
		keyGs = true;
		note = 8;
		newPress = true;
	}
	if (!bitRead(ttouched, 6) && (keyGs)) {
		keyGs = false;
		newPress = false;
	}

	if (bitRead(btouched, 2) && (!keyA)) {
		keyA = true;
		note = 9;
		newPress = true;
	}
	if (!bitRead(btouched, 2) && (keyA)) {
		keyA = false;
		newPress = false;
	}

	if (bitRead(ttouched, 7) && (!keyAs)) {
		keyAs = true;
		note = 10;
		newPress = true;
	}
	if (!bitRead(ttouched, 7) && (keyAs)) {
		keyAs = false;
		newPress = false;
	}

	if (bitRead(btouched, 1) && (!keyB)) {
		keyB = true;
		note = 11;
		newPress = true;
	}
	if (!bitRead(btouched, 1) && (keyB)) {
		keyB = false;
		newPress = false;
	}

	//
	//	Gate and CV out.
	//

	if (newPress) {
		digitalWrite(gate, HIGH);

		vout = tunedNote[(note)+(octave * 12)];

		level = (uint16_t)vout;
		dac.setVoltage(level);
		temp = tcap.readRegister(116);
		bitSet(temp, 0);
		tcap.writeRegister(116, int(temp));
	}

	if (!newPress) {
		if (!latched) {
			digitalWrite(gate, LOW);
			temp = tcap.readRegister(116);
			bitClear(temp, 0);
			tcap.writeRegister(116, int(temp));
		}
	}
}

/*
						TUNING and CONFIGURATION

	Uses a multipoint tuning scheme which is more accurate and less frustrating
	than shift/scale algos.

		Simultaneously touch LATCH, UP, and DOWN to enter tuning mode.

		LEDs will flash, then LATCH LED will "breathe"

		-Octave display shows which octave is currently being tuned.
		-Press F note (center key) to advance through octaves.
		-Press Octave UP and DOWN to tune.
		-Tune 1st position to .083V or C#0
		-1st - 1V or C1
		-2nd - 2V or C2
		-3rd - 3V or C3
		-4th - 4V or C4
		-5th - 4.9167V or B4 (OCTAVE LIGHT WILL GO OUT)
		-Cycle starts over.
		-Press LATCH to save and exit.

*/
void testMode()
{
	//	Flash leds

	for (int i = 0; i < 2; i++)
	{
		tcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
		bcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
		delay(100);
		tcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
		bcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
		delay(100);
	}
	btouched = bcap.touched();
	while (bitRead(btouched, 5) || bitRead(btouched, 6) || bitRead(btouched, 7))
	{
		tcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
		bcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
		delay(100);
		tcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
		bcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
		btouched = bcap.touched();
		delay(100);
	}

	oldOctave = 99;		//	Dirty LEDs to force calculate new values
	octave = 0;			//	Set starting location
	boolean changed = false;
	while (testing)
	{
		digitalWrite(gate, HIGH);

		temp = tcap.readRegister(116);
		bitSet(temp, 0);
		tcap.writeRegister(116, int(temp));

		//	Turn off Latch LED if on.
		temp = bcap.readRegister(116);
		bitClear(temp, 7);
		bcap.writeRegister(116, int(temp));

		//tcap.writeRegister(0x81, int(temp));	//	LED Behaviour Register 1
		temp = bcap.readRegister(130);
		bitSet(temp, 6);
		bcap.writeRegister(130, int(temp));		//	LED Behaviour Register 1
		temp = bcap.readRegister(130);
		bitSet(temp, 7);
		bcap.writeRegister(130, int(temp));		//	LED Behaviour Register 2

		bcap.writeRegister(134, 32);			//	LED Breathe length

		temp = bcap.readRegister(116);
		bitSet(temp, 7);
		bcap.writeRegister(116, int(temp));

		btouched = bcap.touched();
		ttouched = tcap.touched();

		//
		//	Octave Up/Down and lights
		//
		if (bitRead(btouched, 6)) {
			adjust[octave] = adjust[octave] - 50;	//	Speed/accuracy of tuning
			changed = true;
		}

		if (bitRead(btouched, 7)) {
			adjust[octave] = adjust[octave] + 50;	//	Ditto
			changed = true;
		}

		//	Set LEDs
		if (oldOctave != octave) {
			octLED = (6 - octave);
			temp = bcap.readRegister(116);
			bitClear(temp, int(oldOctLED));
			bitSet(temp, int(octLED));
			bcap.writeRegister(116, int(temp));
			oldOctave = octave;
			oldOctLED = octLED;
		}

		//
		//	Simultaneously touch LATCH, UP, and DOWN to reset tuning to defaults.
		//

		if (bitRead(btouched, 5) && bitRead(btouched, 7) && bitRead(btouched, 8)) {
			setPointTuned[0] = 0;
			setPointTuned[1] = 1000000;
			setPointTuned[2] = 2000000;
			setPointTuned[3] = 3000000;
			setPointTuned[4] = 4000000;
			setPointTuned[5] = 5000000;
			memset(setPointUntuned, 0, sizeof(setPointUntuned));
			memset(adjust, 0, sizeof(adjust));

			//	Don't exit until all keys are released to prevent unwanted presses
			while (bitRead(btouched, 5) || bitRead(btouched, 6) || bitRead(btouched, 7))
			{
				tcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
				bcap.writeRegister(CAP1188_LEDOUTCONTROL, 255);
				delay(100);
				tcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
				bcap.writeRegister(CAP1188_LEDOUTCONTROL, 0);
				btouched = bcap.touched();
				delay(100);
			}
			delay(1000);
			testing = false;
		}

		//
		//	Latch used here to exit
		//
		if (bitRead(btouched, 5) && !keyLatch && !bitRead(btouched, 7)) {
			keyLatch = true;
			testing = false;
		}
		if (!bitRead(btouched, 5) && keyLatch) {
			keyLatch = false;
		}

		//	F key to advance octave

		if (bitRead(btouched, 4) && (!keyF)) {
			keyF = true;
			octave++;
			if (octave == 6) {
				octave = 0;
			}
			changed = true;
		}

		if (!bitRead(btouched, 4) && (keyF)) {
			keyF = false;
		}
		/*

		Use a trick here to accurately tune the lowest and highest notes, which are out of range
		of the DAC or beyond the rails of the opamp.

		The next note (which is within the DAC range and rails) is tuned, and the missing note
		is calculated by adding or subtracting .08333V -- the 1V/Octave voltage between notes.

		*/

		delay(25);	//	Let voltages settle

		if (octave == 0) {
			over = ((octave * 1000000) + 83333) + adjust[octave]; // Adjust 1st note to .08333V
			level = over / 1221;	//	Send corrected to DAC
			vout = over - 83333;	//	Back-calculate to zero by subtracting one note value.
		}
		else if (octave == 5) {
			over = ((octave * 1000000) - 83333) + adjust[octave];	//	Adjust 59th note to 4.9166V
			level = over / 1221;	//	Send corrected to DAC
			vout = over + 83333;	//	Calculate to 5V by adding one note value.
		}
		else {
			vout = ((octave * 1000000)) + adjust[octave];
			level = vout / 1221;
		}

		if (level <= 0) { level = 0; }
		if (level >= 4095) { level = 4095; }

		dac.setVoltage(level);

		if (changed) {
			setPointTuned[octave] = vout;
			changed = false;
		}
		//delay(25);
	}

	//
	//	Using now tuned octaves, stretch/compress in between values to fit.
	//

	int index = 0;
	long microvolts;
	long octaveNote;
	for (octave = 1; octave <= 6; octave++)
	{
		for (octaveNote = 0; octaveNote < 12; octaveNote++)
		{
			//microvolts = (((octave - 1) * 1000000) + ((octaveNote) * 1000000) / 12);
			microvolts = index * 83333L;
			tunedNote[index] = (stretch(microvolts, setPointUntuned[octave - 1], setPointUntuned[octave], setPointTuned[octave - 1], setPointTuned[octave]) / 1221L);	//	Phew!

			//	Don't write invalid values to storage.
			if (tunedNote[index] <= 0) { tunedNote[index] = 0; }
			if (tunedNote[index] >= 4095) { tunedNote[index] = 4095; }
			index++;
		}
	}

	//
	//	Exit test mode. Save settings to eeprom and exit.
	//

	//Data to store.
	tunedNote[0] = 0;
	eeAddress = 0;
	for (int i = 0; i < 60; i++) {
		EEPROM.put(eeAddress, tunedNote[i]);
		eeAddress += sizeof(int);
	}

	octave = 2;
	oldOctave = 99;
	latched = false;
	digitalWrite(gate, LOW);
	temp = tcap.readRegister(116);
	bitClear(temp, 0);
	tcap.writeRegister(116, int(temp));

	//
	// Turn off Breathing.
	//

	//	Turn off Latch LED if on.
	temp = bcap.readRegister(116);
	bitClear(temp, 7);
	bcap.writeRegister(116, int(temp));

	temp = bcap.readRegister(130);
	bitClear(temp, 6);
	bcap.writeRegister(130, int(temp));	//	LED Behaviour Register 1
	temp = bcap.readRegister(130);
	bitClear(temp, 7);
	bcap.writeRegister(130, int(temp));	//	LED Behaviour Register 2
	
	temp = bcap.readRegister(116);
	bitSet(temp, 7);
	bcap.writeRegister(116, int(temp));
	temp = bcap.readRegister(116);
	bitClear(temp, 7);
	bcap.writeRegister(116, int(temp));
}

//
//	Rubberband stretch interpolation
//
long stretch(long x, long in_min, long in_max, long out_min, long out_max)
{
	return (x - in_min) * (long long)(out_max - out_min) / (long long)(in_max - in_min) + out_min;
}